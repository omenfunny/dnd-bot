﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace HTTP
{
    class H2Finder
    {
        public string htmlstuff;
        WebClient wc = new WebClient(); //http client
        public string tempinput;
        public bool success = false;

        public void site(string input)
        {
            input = removespaces(input); //remove spaces
            tempinput = input;
            try
            {
                if (input != "") //check if the input is just nothing, because /dnd5e/ is an actual page
                {
                    htmlstuff = wc.DownloadString("https://roll20.net/compendium/dnd5e/" + input); //TARGET SITE 
                }
                else
                {
                    Console.WriteLine("Page does not exist.");
                    htmlstuff = "Page Not Found.";
                }

            }
            catch
            {
                Console.WriteLine("Page does not exist.");
                htmlstuff = "Page Not Found.";
            }
        }


        public void topics()
        {
            Console.WriteLine("\n"); //start it with a new line
            String edit;
            int index = 0;
            String temp = htmlstuff;

            while ((index = temp.IndexOf("<h2")) != -1)
            {
                if (temp.Contains("<script")) //if it has script, stop there because it fucks shit up otherwise
                {
                    temp = getBetween(htmlstuff.ToLower(), "<h2", "<script", htmlstuff); //Makes it not loop back to the top and removes attributes from bottom
                }// if (temp.Contains("<script"))

                edit = getBetween(temp.ToLower(), "<h2", "</h2>", temp); //sets the output string before the regex takes out 

                if (edit.Contains(">\n")) //because scripts, i had to remove stuff if it has the newline thingy in it to avoid random linebreaks
                {
                    edit = getBetween(temp.ToLower(), "<h2", ">\n", temp); //grab text between starting h2 and line break
                }// if (edit.Contains(">\n"))

                edit = Regex.Replace(edit, "<br>", ""); //replace linebreaks with empty strings
                string expn = "<.*?>"; //target html tags
                edit = Regex.Replace(edit, expn, string.Empty); //replace html tags with nothing
                string expn2 = "</a"; //target html tags
                edit = Regex.Replace(edit, expn2, string.Empty); //replace broken a tags with nothing
                string expn1 = "<h2"; //target html tags
                edit = Regex.Replace(edit, expn1, string.Empty); //replace broken h2 tags with nothing
                edit = fixsymbols(edit);

                if (edit != "Compendium" && edit != "") //remove compendium, it comes up twice
                {
                    Console.WriteLine(edit); //display the topics, table will be put in later
                    success = true;
                } //if (edit != "Compendium" && edit != "")
                try
                {
                  temp = temp.Remove(index, "<h2".Length); //remove the h2 thing
                    if (temp.Contains(tempinput) == false)
                    {
                        Console.Clear(); 
                        Console.WriteLine("The input could either not be found, or is in a paid section of the website.");
                        success = false; 
                        break; //drop out of the loop
                    }
                }
                catch
                {
                    //nothing really needs to go here :/
                }
                index++; //increment index.

            }// while ((index = temp.IndexOf("<h2")) != -1)

            Console.WriteLine("\n"); //new line after displaying topics
        } //public void topics()


        public string h2(string input)
        {
            input = input + "</h2>";
            //finds the text in your search, the ending string is an html tag so we know when to stop reading
            //htmlstuff is lowercase manually, will maybe fix in the future, this is easier for now
            String edit;
            try
            {
                edit = getBetween(htmlstuff.ToLower(), input, "<h2", htmlstuff); //sets the output string before the regex takes out html
            }
            catch
            {
                edit = getBetween(htmlstuff.ToLower(), input, "<h1", htmlstuff); //sets the output string before the regex takes out html
            }

            if (edit.Contains("<script")) //if it has script, stop there because it fucks shit up otherwise

            {
                edit = getBetween(htmlstuff.ToLower(), input, "<script", htmlstuff); //Makes it not loop back to the top and removes attributes from bottom
            }


            if (edit != "") //if input is not found, "" is returned so if that isnt returned then run the code
            {
                //so when the list of possible things for a class pops up, the level table will the top option
                if (edit.Contains("<table") && edit.Contains("Table: The"))
                {
                    edit = getBetween(htmlstuff.ToLower(), input, "<table", htmlstuff); //makes sure the table doesnt come in with equipment
                    edit = Regex.Replace(edit, "<li>", "\n");
                }

                if (edit.Contains("<h4>")) //this helps with formatting
                {
                    edit = Regex.Replace(edit, "<h4>", "\n");
                    edit = Regex.Replace(edit, "</h4>", ":\n");
                }
                if (edit.Contains("<h3>")) //as does this.
                {
                    edit = Regex.Replace(edit, "</h3>", ":\n");
                }

                edit = Regex.Replace(edit, "<br>", "\n");
                string expn = "<.*?>"; //target html tags
                edit = Regex.Replace(edit, expn, string.Empty); //replace html tags with nothing
                edit = fixsymbols(edit);

                int space = input.Length - 5; //this takes the length of the input and subtracts 5 because i hardcoded </h2>
                edit = edit.Insert(space, ": \n\n"); //make 2 new lines and a colon for the search so it looks nicer
                Console.Clear(); //clears console to get rid of the question and search query
                return (edit); //output the result
            }
            else
            {
                return ("Could not find shit bro"); //tell user that nothing was found
            }
        } //public string h2(string input)

        private string removespaces(string input) //removes spaces.
        {
            input = input.Replace(" ", string.Empty);
            return input;
        } //private string removespaces(string input)

        private string fixsymbols(string input) //replaces broken symbols with gud ones
        {
            input = Regex.Replace(input, "&quot;", "\"");  //replace broken quotations with good ones
            input = Regex.Replace(input, "â€™", "\'"); //replace broken slashes with good ones
            input = Regex.Replace(input, "â€¢", "*"); //replace asterisks slashes with good ones
            input = Regex.Replace(input, "â€”", " -- "); //replace broken dashes with good ones
            return input;
        }//private string fixsymbols(string input)

        public static string getBetween(string strSource, string strStart, string strEnd, string strOSauce) //amazing function for finding text in a search
        {
            int Start, End; //start and end ints
            if (strSource.Contains(strStart) && strSource.Contains(strEnd)) //check to see if text contains the search so we dont crash
            {
                Start = strSource.IndexOf(strStart, 0); //finds the user inputed search, cuts it out and sets a starting point
                End = strSource.IndexOf(strEnd, Start); //finds end of search, this will be hard coded since we have a web template
                return strOSauce.Substring(Start, End - Start); //return the text
            }
            else
            {
                return ""; //return nothing if we find nothing
            }

        } // public static string getBetween(string strSource, string strStart, string strEnd)
    } //class H2Finder
} //namespace HTTP
