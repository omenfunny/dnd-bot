﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;
namespace HTTP
{
    //TODO: Tables and a list of races and classes at the start, on command since everything in linear so far

    class Program
    {
        static void Main(string[] args)
        {
            H2Finder findH2 = new H2Finder(); //call h2find class
            Console.WriteLine("What class or race do you want?"); //asks user what they want
            findH2.site(Console.ReadLine()); //read input for site

            if (!findH2.htmlstuff.Contains("Page Not Found.")) //if it doesnt return page not found then you can type stuffs
            {
                findH2.topics(); //display topics
                if (findH2.success == true)
                {
                    Console.WriteLine("Ask a question"); //prompts user to ask a question
                    Console.WriteLine(findH2.h2(Console.ReadLine())); //read input for text
                }

            } //if (!findH2.htmlstuff.Contains("Page Not Found."))

            Console.WriteLine("Press Any Key To Continue...");
            Console.ReadKey(); //press any key to continue
        }//static void Main(string[] args)
    }//class Program
}//namespace HTTP
